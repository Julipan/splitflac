#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Stian Rødven Eide

import enchant


def fix_english(string, dictionary="en_UK"):
    d = enchant.Dict(dictionary)
    words = string.split()
    checkwords = words
    parantwords = []
    for i,word in enumerate(words):
        if word.startswith("(") or word.startswith("["):
            checkwords = words[:i]
            parantwords = words[i:]
            break
    trues = 0
    falses = 0
    for word in checkwords:
        if d.check(word):
            trues += 1
        else:
            falses += 1
    if trues < falses:
        if dictionary == "en_UK":
            fix_english(string, dictionary="en_US")
        string = " ".join(words)
        return string
    # If we get here, it is probably an english string
    lowwords = ["a", "an", "the", "for", "and", "of", "on",
                "but", "or", "to", "at", "in", "with", "as"]
    fixedwords = []
    for i,word in enumerate(checkwords):
        if i == 0 or i == (len(checkwords) - 1):
            word = word[0].upper() + word[1:].lower()
        elif word.lower() in lowwords:
            word = word.lower()
        else:
            word = word[0].upper() + word[1:].lower()
        fixedwords.append(word)
    string = " ".join(fixedwords + parantwords) 
    return string


if __name__ == '__main__':
    string = input("Write: ")
    print(fix_english(string))
