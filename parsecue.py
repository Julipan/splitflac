#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Stian Rødven Eide

import os
import sys
import re

class CueSheet:
    """Parses a cuesheet with the method parse_cue().
       The following fields will then be callable:
       .performer (album artist)
       .title     (album title)
       .date      (year of release)
       .genre     (e.g. Progressive Rock)
       .comment   (often an ad set by the ripping program)
       .file      (the audio file accompanying the cue file)
       The above are metadata for the album as a whole.
       
       In addition, a dictionary with track information is available 
       from the field .tracks, the keys for which are:
        TRACK     (track number)
        PERFORMER (song artist)
        TITLE     (song title)
        INDEX     (time code for start of song)
       Metadata for each track is available through the 
       method .track(number)[KEY]
       
       Lastly, the method .cuebreakpoints() will return a string of
       all track indexes, except the first one, separated by a newline.
       This is the format expected by shntools.
    """
    def __init__(self):
        self.genre = ''
        self.date = ''
        self.comment = ''
        self.performer = ''
        self.title = ''
        self.file = ''
        self.tracks = {}
        
    def parse_cue(self, cuedata):
        """Parses a cuesheet with regular expressions. Sets the fields
           declared in __init__.
        """
        # re_alb matches everything up to but not including the first 
        # indented line. This should be all album specific information.
        # Use re_alb with re.DOTALL
        re_alb = "^.*?(?=\n\s)"
        re_per = "PERFORMER \"*(.+?)\"*\n"
        re_tit = "TITLE \"*(.+?)\"*\n"
        re_dat = "DATE \"*(\d+)"
        re_gen = "GENRE \"*(.+?)\"*\n"
        re_com = "COMMENT \"*(.+?)\"*\n"
        re_fil = "FILE \"*(.+?)\"*\n"
        # Track specific regular expressions.
        # re_tra matches any data starting from TRACK until it is
        # superceeded by either the next TRACK or an EOF.
        # Use re_tra with re.DOTALL
        re_tra = "TRACK.+?(?=\sTRACK|$)"
        re_tnr = "TRACK (\d+) AUDIO"
        re_tix = "INDEX 01 ([\d:]{8})"
        try:
            album_info = re.match(re_alb, cuedata, re.DOTALL).group(0)
        except AttributeError:
            album_info = ''
        try:
            self.performer = re.search(re_per, album_info).group(1)
        except AttributeError:
            self.performer = ''
        try:
            self.title = re.search(re_tit, album_info).group(1)
        except AttributeError:
            self.title = ''
        try:
            self.date = re.search(re_dat, album_info).group(1)
        except AttributeError:
            self.date = ''
        try:
            self.genre = re.search(re_gen, album_info).group(1)
        except AttributeError:
            self.genre = ''
        try: 
            self.comment = re.search(re_com, album_info).group(1)
        except AttributeError:
            self.comment = ''
        try:
            self.file = re.search(re_fil, album_info).group(1)
        except AttributeError:
            self.file = ''
        try:
            tracks = re.findall(re_tra, cuedata, re.DOTALL)
        except AttributeError:
            tracks = []
        all_tracks = {}
        for t in tracks:
            track_dict = {}
            tnumber = re.search(re_tnr, t).group(1)
            track_dict["TRACK"] = tnumber
            ttitle = re.search(re_tit, t).group(1)
            track_dict["TITLE"] = ttitle
            try:
                tperformer = re.search(re_per, t).group(1)
                track_dict["PERFORMER"] = tperformer
            except AttributeError:
                pass
            tindex = re.search(re_tix, t).group(1)
            track_dict["INDEX"] = tindex
            all_tracks[int(tnumber)] = track_dict
        self.tracks = all_tracks
        
    def test(self):
        """Returns True if there are any tracks, else raises a
           FileNotFoundError.
        """
        if self.tracks:
            return True
        else:
            raise FileNotFoundError("No cue data!")
        
    def track(self, number):
        """Give it a number, and this method will return a dictionary
           with metadata for a track with that number. If no such track
           exists, it returns None.
        """
        if number in self.tracks:
            return self.tracks[number]
        else:
            return None
            
    def cuebreakpoints(self):
        """Returns a string of track indexes in the format expected
           by shntools.
        """
        brlist = []
        # We skip the first index, which usually is 00:00:00
        for i in range(len(self.tracks)-1):
            point = self.track(i+2)["INDEX"]
            brlist.append(point)
        breakpoints = "\n".join(brlist)
        return breakpoints

if __name__ == "__main__":
    try:
        cuefile = sys.argv[1]
        f = open(cuefile)
        cuesheet = CueSheet()
        cuesheet.parse_cue(f.read())
        print("Performer: %s" % cuesheet.performer)
        print("Album: %s" % cuesheet.title)
        print("Year: %s" % cuesheet.date)
        print("Genre: %s" % cuesheet.genre)
        print("Comment: %s" % cuesheet.comment)
        print("Example track:")
        print(cuesheet.track(2))
        print("Cue breakpoints:")
        print(cuesheet.cuebreakpoints())
        print(cuesheet.tracks)
    except IndexError:
        print("No cue file specified or invalid syntax")
    except UnicodeDecodeError:
        print("Not a valid cue file")

